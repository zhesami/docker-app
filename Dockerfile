FROM bitnami/laravel:latest

WORKDIR /projects/app

COPY . .

RUN npm install

RUN composer install

CMD php artisan serve

EXPOSE 8000
